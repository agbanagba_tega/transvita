package com.transvita;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Handles view click events in the navigation drawer.
 */
public class NavListClickListener implements View.OnClickListener {

    /**/
    private RecyclerView recyclerView;

    /**/
    private Context context;

    private DrawerLayout drawerLayout;


    /**
     * Constructor of the navigation list click listener that takes the context of the application
     * and the recycler view of the navigation list to get the view clicked.
     *
     * @param context      the context.
     * @param recyclerView the recycler view of the navigation list.
     */
    public NavListClickListener(Context context, RecyclerView recyclerView, DrawerLayout drawerLayout) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.drawerLayout = drawerLayout;
    }

    @Override
    public void onClick(View v) {
        selectItem(recyclerView.getChildLayoutPosition(v));
    }

    /**
     * Based on the position of the clicked view, the app responds to the view clicked accordingly, opening
     * activities or fragments that are necessary.
     *
     * @param position the position of the clicked view.
     */
    private void selectItem(int position) {
        Intent intent = null;
        switch (position) {
            // Case 1 represents the profile page of the user.
            case 1:
                intent = new Intent(context, ProfileActivity.class);
                break;

            // Case 2 links to the users reservation history.
            case 2:
                intent = new Intent(context, HistoryActivity.class);
                break;

            // Case 3 links to the payments page of the user.
            case 3:
                intent = new Intent(context, PaymentsActivity.class);
                break;

            // Case 4 opens the settings page of the application.
            case 4:
                intent = new Intent(context, SettingsActivity.class);
                break;
        }
        // Start the activity of the clicked view.
        context.startActivity(intent);
        drawerLayout.closeDrawer(recyclerView);
    }
}