package com.transvita;

import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import model.Constants;
import model.Utility;


public class AppMapFragment extends FragmentActivity implements
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    /* Private Instance Variables */
    private GoogleApiClient mGoogleApiClient;

    /* Current location latitude and longitude. */
    private double latitude;
    private double longitude;

    /* An instance to the map object. */
    private GoogleMap mGoogleMap;

    private Location mLocation;
    private AddressResultReceiver mResultReceiver;

    /* The current address of the user. */
    private String currentAddressLocation;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_map);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        buildGoogleApiClient();
        mGoogleApiClient.connect();

        /** Sets the floating action button and the click listener */
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        try {
            fab.setOnClickListener(new FABClickListener(this, findViewById(R.id.coordinator_layout)));
        } catch (Exception ex) {
            Log.e(getResources().getString(R.string.app_name),
                    "When calling FABClickListener for floating action button, the view to be passed in must " +
                            "be a coordinator layout.");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
    }

    /**
     * Builds the google api client and adds necessary callbacks.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this,"Connected",Toast.LENGTH_LONG).show();
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            mLocation = location;
            // Geocoder not present on device
            if (!Geocoder.isPresent())
                return;

            startAddressLookupService();
        }

        Log.i("LongLat",latitude + "," + longitude);

        // Current user location.
        LatLng currentLocation = new LatLng(latitude, longitude);

        // get the location of the device and zoom to the location.
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
    }

    /**
     * Delivers the address from the address receiver to the map activity for marker and
     * current location display.
     *
     * @param address the address to deliver.
     */
    private void deliverResultToMapActivity(String address) {
        currentAddressLocation = address;
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                .title(currentAddressLocation));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * Repositions the my location button and places it at the bottom of the screen.
     * This method is no longer used since the floating action button will cover the location
     * layer button.
     *
     * @param mapView the map view.
     */
    @Deprecated
    private void rePositionLocationButton(View mapView) {
        View locationButton = ((View) mapView.findViewById(1).getParent()).findViewById(2);
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
    }

    /**
     * Begins the address look up service on a separate thread in
     * the background and waits for the result.
     */
    protected void startAddressLookupService() {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        mResultReceiver = new AddressResultReceiver(new Handler());
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLocation);
        this.startService(intent);
    }

    /**
     * Floating Action Button Click Listener that listens to events for the floating action bar
     * and the snackbar in  the Map UI.
     */
    private class FABClickListener implements View.OnClickListener {

        /* Private Instance Variables */
        private View coordinatorView;
        private Context context;

        /**
         * A constructor to create a new floating action bar with a coordinator layout.
         * for the snackbar in cases of error
         *
         * @param context the context which this is applied.
         * @param view    the coordinator layout.
         * @throws Exception when the view provided is not an instance of Coordinator layout.
         */
        public FABClickListener(Context context, View view) throws Exception {
            if (!(view instanceof CoordinatorLayout))
                throw new Exception("The view provided is not an instance of Coordinator Layout");
            coordinatorView = view;
            this.context = context;
        }


        @Override
        public void onClick(View v) {
            /* If there is no connection, display the snackbar showing no internet connection. */
            if (!Utility.isInternetConnected(context)) {
                Snackbar snack = Snackbar.make(coordinatorView, R.string.no_internet, Snackbar.LENGTH_LONG);
                ((TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text))
                        .setTextColor(context.getResources().getColor(R.color.white));
                snack.show();
            } else {
                // TODO: Launch whatever will start the reservation process.
            }
        }
    }

    /**
     * The address result receiver receives the result of the address look up
     * intent service performed on a separate thread.
     */
    private class AddressResultReceiver extends ResultReceiver {

        /* Private Instance variables */
        private String addressString;

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                addressString = resultData.getString(Constants.RESULT_DATA_KEY);
                deliverResultToMapActivity(addressString);
            } else if (resultCode == Constants.FAILURE_RESULT)
                addressString = getResources().getString(R.string.no_address_found);
        }
    }
}