package com.transvita;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;

public class FadeInOutPageTransformer implements ViewPager.PageTransformer {

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        // Image view to transform with the fade in fade out.
        View imageView = null;
        int imageViewLocation = 0;

//        View contentView = view.findViewById(R.id.content_area);

        // Get all the child views in the view layout
        RelativeLayout layoutView = (RelativeLayout) view;
        View[] childViews = new View[layoutView.getChildCount()];

        // Child view of the layout view
        View childView = null;
        for (int i = 0; i < layoutView.getChildCount(); i++) {
            childView = layoutView.getChildAt(i);
            childViews[i] = childView;
        }

        // Get the image view from layout view child views.
        for (int i = 0; i < childViews.length; i++) {
            if (childViews[i].getId() == R.id.view_pager_image) {
                imageView = childViews[i];
                imageViewLocation = i;
            }
        }

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left
        } else if (position <= 0) { // [-1,0]
            // This page is moving out to the left

            // Counteract the default swipe
            view.setTranslationX(pageWidth * -position);
//            if (contentView != null) {
            // But swipe the contentView
            for (int i = 0; i < childViews.length; i++) {
                if (i == imageViewLocation)
                    continue;
                childViews[i].setTranslationX(pageWidth * position);
            }
//            }
            if (imageView != null) {
                // Fade the image in
                imageView.setAlpha(1 + position);
            }

        } else if (position <= 1) { // (0,1]
            // This page is moving in from the right

            // Counteract the default swipe
            view.setTranslationX(pageWidth * -position);
//            if (contentView != null) {
            // But swipe the contentView. Content view is everything apart from the image view.
            for (int i = 0; i < childViews.length; i++) {
                if (i == imageViewLocation)
                    continue;
                childViews[i].setTranslationX(pageWidth * position);
            }
//            }
            if (imageView != null) {
                // Fade the image out
                imageView.setAlpha(1 - position);
            }
        } else { // (1,+Infinity]
            // This page is way off-screen to the right
        }
    }
}