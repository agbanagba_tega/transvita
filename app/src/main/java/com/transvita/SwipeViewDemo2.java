package com.transvita;

import android.app.Activity;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;

import uk.co.jasonfry.android.tools.ui.PageControl;
import uk.co.jasonfry.android.tools.ui.SwipeView;


public class SwipeViewDemo2 extends AppCompatActivity {

    private SwipeView mSwipeView;
    private int[] images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_view_demo2);

        PageControl mPageControl = (PageControl) findViewById(R.id.page_control);
        mSwipeView = (SwipeView) findViewById(R.id.swipe_view);

        loadImages();

        for (int i = 0; i < 7; i++)
            mSwipeView.addView(new FrameLayout(this));

        ImageView image1 = new ImageView(this);
        ImageView image2 = new ImageView(this);
        image1.setImageResource(images[0]);
        image2.setImageResource(images[1]);

        ((FrameLayout) mSwipeView.getChildContainer().getChildAt(0)).addView(image1);
        ((FrameLayout) mSwipeView.getChildContainer().getChildAt(1)).addView(image2);

        SwipeImageLoader mSwipeImageLoader = new SwipeImageLoader();
        mSwipeView.setOnPageChangedListener(mSwipeImageLoader);
        mSwipeView.setPageControl(mPageControl);
    }

    private class SwipeImageLoader implements SwipeView.OnPageChangedListener {

        /**
         * What this method is really doing is saving ones ass from memory overflow or ANR's.
         * It releases images from the app on swipe out and loads it back on sign in.
         *
         * @param oldPage
         * @param newPage
         */
        public void onPageChanged(int oldPage, int newPage) {
            if (newPage > oldPage) {
                if (newPage != mSwipeView.getPageCount() - 1) {
                    ImageView v = new ImageView(SwipeViewDemo2.this);
                    v.setImageResource(images[newPage + 1]);

                    ((FrameLayout) mSwipeView.getChildContainer().getChildAt(newPage + 1)).addView(v);
                }

                if (oldPage != 0)
                    ((FrameLayout) mSwipeView.getChildContainer().getChildAt(oldPage - 1)).removeAllViews();
            } else {
                if(newPage != 0){
                    ImageView v = new ImageView(SwipeViewDemo2.this);
                    v.setImageResource(images[newPage - 1]);
                    ((FrameLayout) mSwipeView.getChildContainer().getChildAt(newPage - 1)).addView(v);
                }

                if(oldPage != mSwipeView.getPageCount() - 1)
                    ((FrameLayout) mSwipeView.getChildContainer().getChildAt(oldPage + 1)).removeAllViews();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_swipe_view_demo2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void loadImages() {
        images = new int[7];
        images[0] = R.drawable.image001;
        images[1] = R.drawable.image002;
        images[2] = R.drawable.image003;
        images[3] = R.drawable.image004;
        images[4] = R.drawable.image005;
        images[5] = R.drawable.image006;
        images[6] = R.drawable.image007;

    }
}
