package com.transvita;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import model.AccountUser;
import model.Application;
import model.QRUser;
import model.User;


public class ProfileActivity extends AppCompatActivity {

    /* Private Instance Variables */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_profile));
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        setSupportActionBar(toolbar);

        AccountUser applicationUser = null;
        try {
            applicationUser = (AccountUser) Application.getApplicationUser();
        } catch (NullPointerException npe) {
            Log.e(Application.Tags.USER_ERROR_TAG, "invalid application user.");
        }

        TextView txtViewFirstName = (TextView) findViewById(R.id.first_name);
        txtViewFirstName.setText(applicationUser.getFirstName());

        TextView txtViewLastName = (TextView) findViewById(R.id.last_name);
        txtViewLastName.setText(applicationUser.getLastName());

        TextView emailAddress = (TextView) findViewById(R.id.email_address);
        emailAddress.setText(applicationUser.getEmailAddress());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
