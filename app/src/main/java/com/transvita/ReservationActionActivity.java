package com.transvita;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;

import model.Application;
import model.ApplicationService;
import model.DataValidator;
import model.DistanceMatrix;
import model.EditTextDataExtractor;
import model.Places;
import model.Request;
import model.RequestData;
import model.ServiceType;
import model.ValidationService;
import widgets.PlacesAutoCompleteTextView;


public class ReservationActionActivity extends AppCompatActivity implements DataValidator {

    /* Private Instance Variables */

    /**
     * The user's location in string either gotten from the pick up editText or location services.
     */
    private String pickUpAddressLocation;

    /**
     * Validation service to validate user data
     */
    private ValidationService _validationService;

    /**
     * Private Instance of the pick up address edit text.
     */
    private AutoCompleteTextView pickUpAddressEditText;

    /**
     * Private instance of the destination address edit text.
     */
    private AutoCompleteTextView destinationAddressEditText;

    /** Progress bar for the pick up location auto complete text view. */
    private ProgressBar pick_up_pbar;

    private static final String PICK_UP_CALLING_VIEW = "pick_up_calling_view";
    private static final String DESTINATION_CALLING_VIEW = "destination_calling_view";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reservation_action);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle(R.string.reservation);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        pick_up_pbar = (ProgressBar)findViewById(R.id.pick_up_progress_bar);
        // instantiate private instances of edit text objects.
        /** Pick up address auto complete text view. */
        pickUpAddressEditText = (PlacesAutoCompleteTextView) findViewById(R.id.pick_up);
        pickUpAddressEditText.addTextChangedListener(new AddressTextWatcher(this, PICK_UP_CALLING_VIEW));

        /** Destination address auto complete text view. */
        destinationAddressEditText = (PlacesAutoCompleteTextView) findViewById(R.id.destination);
        destinationAddressEditText.addTextChangedListener(new AddressTextWatcher(this, DESTINATION_CALLING_VIEW));

        findViewById(R.id.getPrice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPriceQuote();
            }
        });

        // Get the user current address in string.
        pickUpAddressLocation = Application.getCurrentAddressLocation();
        ((EditText) findViewById(R.id.pick_up)).setText(pickUpAddressLocation);
        // Extract destination data and Register a validation service to validate reservation data.
        int[] editTextIds = {R.id.pick_up, R.id.destination};
        EditTextDataExtractor _dataExtractor = new EditTextDataExtractor(this, editTextIds);

        _validationService = (ValidationService) ApplicationService.getInstance(ServiceType.ValidationService);
        try {
            _validationService.register(this, findViewById(R.id.confirmReservation),
                    ValidationService.LISTENER_VIEW_ON_CLICK, _dataExtractor);
        } catch (Exception ex) {
            Log.e(Application.Tags.USER_ERROR_TAG, "Activity did not implement DataValidator.");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void validate(String[] data) {

        /**
         * On click listener for the confirm button which sends the reservation information.
         * After the confirm reservation button is clicked, the app checks for the users payment
         * preference and if always asks, brings up a dialog for the user to choose payment
         * preference for the current reservation.
         */


        // Checking for empty strings as the address.
        String pick_up = data[0];
        String dest = data[1];
        if (pick_up.equalsIgnoreCase(""))
            pickUpAddressEditText.setError("Please enter a pick up location.");
        else if (dest.equalsIgnoreCase(""))
            destinationAddressEditText.setError("Please enter a destination.");

        // TODO Check preference information and show different dialogs.

        // Make a reservation request to the server.
        RequestData requestData = new RequestData(Application.Tags.APPLICATION_RESERVATION);

        // Request data params for the reservation request are (User information, Pick up address, Destination Address)
        // TODO An alternative data that can also be sent is the payment information.
        requestData.params(pick_up, dest);
        new Request(this, Application.Tags.APPLICATION_RESERVATION).execute(requestData);

        // Finish The Activity.
        finish();
    }

    /**
     * Gets the distance between pick up location and destination and queries
     * the database for billing information.
     */
    private void getPriceQuote() {
        String pickup = pickUpAddressEditText.getText().toString();
        String destination = destinationAddressEditText.getText().toString();

        if (pickup.equalsIgnoreCase(""))
            pickUpAddressEditText.setError("Please enter a pick up location.");

        if (destination.equalsIgnoreCase(""))
            destinationAddressEditText.setError("Please enter a destination.");

        /** Get the distance between the two places. */
        DistanceMatrix distanceMatrix = new DistanceMatrix(pickup, destination);
        distanceMatrix.execute();

    }

    /**
     * Address text watcher is the text watcher for the pick up address and destination
     * address auto complete text views.
     *
     * @author Agbanagba Oghenetega
     */
    private class AddressTextWatcher implements TextWatcher {

        /* Private Instance Variables */
        private Context context;
        private String callingView;

        public AddressTextWatcher(Context context, String callingView) {
            this.context = context;
            this.callingView = callingView;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (callingView) {
                case PICK_UP_CALLING_VIEW:
                    new Places.PlacesTask(context, pickUpAddressEditText).execute(s.toString());
                    break;
                case DESTINATION_CALLING_VIEW:
                    new Places.PlacesTask(context, destinationAddressEditText).execute(s.toString());
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}