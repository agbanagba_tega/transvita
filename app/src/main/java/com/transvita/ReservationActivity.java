package com.transvita;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import adapters.RecyclerViewAdapter;
import model.AccountUser;
import model.Application;
import model.Constants;
import model.Utility;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;


/**
 * The reservation activity showing the map ui and navigation drawer for navigating to other activities.
 */
public class ReservationActivity extends AppCompatActivity implements OnMapReadyCallback {

    /* Private Instance Variables */

    private DrawerLayout mDrawerLayout;
    private RecyclerView mRecyclerView;

    private ActionBarDrawerToggle mActionBarDrawerToggle;

    private String[] mNavDrawerItems;

    private FloatingActionButton floatingActionButton;

    /* Current location latitude and longitude. */
    private double latitude;
    private double longitude;

    /* An instance to the map object. */
    private static GoogleMap mGoogleMap;

    /* The current address of the user. */
    private String currentAddressLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        // Performs the same function as an action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(toolbar);

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.content_frame);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (savedInstanceState == null)
            mapFragment.getMapAsync(this);

        /** Sets the floating action button and the click listener */
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        try {
            floatingActionButton.setOnClickListener(new FABClickListener(this, findViewById(R.id.content_frame)));
        } catch (Exception ex) {
            Log.e(getResources().getString(R.string.app_name),
                    "When calling FABClickListener for floating action button, the view to be passed in must " +
                            "be a coordinator layout.");
        }

        // A snackbar also shows when the user is trying to make a reservation saying
        // there is no internet connection.

        // Check the network connection state and show a snackbar if there is no network connection
        if (Utility.isInternetConnected(this))
            Snackbar.make(coordinatorLayout, R.string.no_internet, Snackbar.LENGTH_LONG).show();

        // Navigation item list array.
        mNavDrawerItems = getResources().getStringArray(R.array.nav_drawer_list_items);
        String[] mNavDrawerFooterItems = getResources().getStringArray(R.array.nav_footer_items);
        int[] navDrawerImages = {R.drawable.ic_profile_normal, R.drawable.ic_media_play, R.drawable.ic_payment_normal, R.drawable.ic_settings_normal};

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.left_drawer);
        mRecyclerView.setHasFixedSize(true);

        AccountUser user = new AccountUser("Oghenetega", "Agbanagba", "tegamike@gmail.com");
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(mNavDrawerItems, navDrawerImages, user, mNavDrawerFooterItems);
        adapter.setViewClickListener(new NavListClickListener(this, mRecyclerView, mDrawerLayout));
        mRecyclerView.setAdapter(adapter);

        // Layout manager for the recycler view.
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        startBeginnersGuide();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reservation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Performs map initialization.
     */
    private void mapInit() {
        Location location = Application.getUserLocation();
        currentAddressLocation = Application.getCurrentAddressLocation();

        // TODO If location data not ready yet wait for it . When location data ready display
        // TODO continue operation.
        try {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        } catch (NullPointerException npex) {
            Log.i(Application.Tags.APPLICATION_ERROR, "User location data not ready.");
        } finally {
            // TODO Perform wait operation

        }

        // Current user location.
        LatLng currentLocation = new LatLng(latitude, longitude);

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_marker);

        // A button that is at the top right of the application when clicked changes to the users location.
        UiSettings uiSettings = mGoogleMap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(true);

//        mGoogleMap.setMyLocationEnabled(true);

        // get the location of the device and zoom to the location.
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
        Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                .title(currentAddressLocation).icon(icon).draggable(true));
        marker.showInfoWindow();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        Log.i(Constants.APP_TAG, "Map Ready called");
        // Zooms the map view to the users location and puts the marker.
        mapInit();
    }

    /**
     * Starts a beginner guide of show case views touching parts of the application with explanation
     * for the user to understand and implement.
     */
    private void startBeginnersGuide() {
        new MaterialShowcaseView.Builder(this)
                .setTarget(floatingActionButton)
                .setDismissText("GOT IT")
                .setContentText(getResources().getString(R.string.showcaseContentView))
                .setContentTextColor(getResources().getColor(R.color.material_grey_500))
                .setMaskColour(getResources().getColor(R.color.material_cyan_500))
                .setDelay(1000)
                .singleUse("SHOWCASE")
                .show();
    }

    /**
     * Floating Action Button Click Listener that listens to events for the floating action bar
     * and the snackbar in  the Map UI.
     */
    private class FABClickListener implements View.OnClickListener {

        /* Private Instance Variables */
        private View coordinatorView;
        private Context context;

        /**
         * A constructor to create a new floating action bar with a coordinator layout.
         * for the snackbar in cases of error
         *
         * @param context the context which this is applied.
         * @param view    the coordinator layout.
         * @throws Exception when the view provided is not an instance of Coordinator layout.
         */
        public FABClickListener(Context context, View view) throws Exception {
            if (!(view instanceof CoordinatorLayout))
                throw new Exception("The view provided is not an instance of Coordinator Layout");
            coordinatorView = view;
            this.context = context;
        }


        @Override
        public void onClick(View v) {
            /* If there is no connection, display the snackbar showing no internet connection. */
            if (!Utility.isInternetConnected(context)) {
                Snackbar snack = Snackbar.make(coordinatorView, R.string.no_internet, Snackbar.LENGTH_LONG);
                ((TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text))
                        .setTextColor(context.getResources().getColor(R.color.white));
                snack.show();
            } else {
                /** Launches the reservation action activity for user to get price quote and distance. */
                Intent intent = new Intent(context, ReservationActionActivity.class);
                context.startActivity(intent);
            }
        }
    }
}