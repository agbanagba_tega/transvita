package com.transvita;

import android.app.Activity;
import android.os.Build;
import android.view.WindowManager;

public class VersionCheck {

    /**
     * Changes the color of the status bar in pre-lollipop devices. The status bar is a system window owned
     * by the operating system and this works.
     * @param activity the activity to which the change is applied.
     * @param color the color to change to.
     */

    public static void statusBarColor(Activity activity,int color){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(color));
        }
    }
}