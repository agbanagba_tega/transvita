package com.transvita;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.viewpagerindicator.CirclePageIndicator;

import adapters.ViewPagerAdapter;
import model.Utility;


public class MainActivity extends AppCompatActivity {

    /* Private Instance Variables */

    /* Number of pages in the view pager. */
    private static final int NUM_PAGES = 2;

    /* The main layout manager for the activity. */
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Show  dialog to enable gps services to use the location services in the app.
        if (!Utility.isGPSEnabled(this))
            Utility.gpsEnableDialog(this).show();

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), NUM_PAGES);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setPageTransformer(true, new FadeInOutPageTransformer());

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator)findViewById(R.id.circlePagerIndicator);
        circlePageIndicator.setViewPager(mViewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Show  dialog to enable gps services to use the location services in the app.
        if (!Utility.isGPSEnabled(this))
            Utility.gpsEnableDialog(this).show();
    }

    @Override
    public void onBackPressed() {
        // On pressing the back button on the device, the view pager should return to the previous page.
        if (mViewPager.getCurrentItem() == 0)
            super.onBackPressed();
        else
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}