package com.transvita;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import fragments.ForgotPasswordFragment;
import model.Application;
import model.ApplicationService;
import model.Constants;
import model.DataValidator;
import model.EditTextDataExtractor;
import model.Request;
import model.RequestData;
import model.Result;
import model.ServiceType;
import model.Utility;
import model.ValidationService;

public class LoginActivity extends FragmentActivity implements DataValidator {

    /* Private Instance Variables */

    /* A validation service used in the application. */
    private ValidationService validationService;

    /* Edit text used to collect email address from the user. */
    private EditText emailEditText;

    /* Edit text used to collect password from user*/
    private EditText passEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Animate the activity into the screen from the bottom.
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.pull_hold);

        TextView txtSignUp = (TextView) findViewById(R.id.clickable_sign_up_text);
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        // Launches the forgot password dialog when the text view is clicked for password retrieval.
        TextView txtForgotPassword = (TextView) findViewById(R.id.forgot_password);
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment forgotPasswordFragment = new ForgotPasswordFragment();
                forgotPasswordFragment.show(getSupportFragmentManager(), "ForgotPasswordFragment");
            }
        });

        // Edit text objects of email and password.
        emailEditText = (EditText) findViewById(R.id.editTxtEmailAddressLogin);

        // When the user puts in the email address and the email is wrong on change focus to
        // another view, an invalid error message is thrown.
        emailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String emailAddress = emailEditText.getText().toString();
                    boolean emailValid = validationService.validateEmail(emailAddress);
                    if (!emailValid || emailAddress.equalsIgnoreCase(""))
                        emailEditText.setError("Please enter a valid email");
                }
            }
        });
        passEditText = (EditText) findViewById(R.id.editTxtPasswordLogin);

        // Extract data from edit text.
        int[] editTextIds = {R.id.editTxtEmailAddressLogin, R.id.editTxtPasswordLogin};
        EditTextDataExtractor _extractor = new EditTextDataExtractor(this, editTextIds);

        // Register a validation service for data validation.
        validationService = (ValidationService) ApplicationService.getInstance(ServiceType.ValidationService);
        try {
            validationService.register(this, findViewById(R.id.btnLoginInLogin),
                    validationService.LISTENER_VIEW_ON_CLICK, _extractor);
        } catch (Exception e) {
            Log.e("ERROR", "Activity did not implement DataValidator");
        }
    }

    @Override
    public void validate(String[] data) {

        /** Collapse the android keyboard */
        Utility.collapseKeyboard(this);

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.snackbar_container);

        String emailAddress = data[0];
        String password = data[1];

        // Performs validation and verification of email address and password with online data.

        // Email Validation
        boolean emailValid = validationService.validateEmail(emailAddress);

        // Password Validation
        boolean passwordValid = validationService.validatePassword(password);

        // Before validating an verifying, presence of internet connection has to be gotten.
        if (!Utility.isInternetConnected(this)) {
            Snackbar snack = Snackbar.make(coordinatorLayout, R.string.no_internet, Snackbar.LENGTH_LONG);
            ((TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text))
                    .setTextColor(getResources().getColor(R.color.white));
            snack.show();
        } else if (emailValid == false || password.equalsIgnoreCase(""))
            emailEditText.setError("Please enter a valid email");
        else if (passwordValid == false || password.equalsIgnoreCase(""))
            passEditText.setError("Please enter a valid password");
        else {

            // Makes a request for account verification.
            RequestData requestData = new RequestData(Application.Tags.APPLICATION_LOGIN);
            requestData.params(emailAddress, password);
            new Request(this, Application.Tags.APPLICATION_LOGIN).execute(requestData);

            // The progress dialog waits for response from the server
//            ProgressDialog progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage(getResources().getString(R.string.logging_in));
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setIndeterminate(true);
//            progressDialog.setCanceledOnTouchOutside(false);
//            progressDialog.show();


            /* Add application user to Application object for
             * application wide access */
//            Application.setApplicationUser(new QRUser());

            // TODO: Verify data in database and start reservation activity.
            Intent intent = new Intent(this, ReservationActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void onPause() {
        // When the activity is paused (i.e it is no longer visible), the activity leaves the screen by a slide
        // through the bottom of the screen.
        overridePendingTransition(R.anim.pull_hold, R.anim.slide_out_bottom);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}