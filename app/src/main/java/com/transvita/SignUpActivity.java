package com.transvita;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import model.AccountUser;
import model.Application;
import model.ApplicationService;
import model.CountryCodes;
import model.DataValidator;
import model.EditTextDataExtractor;
import model.RequestData;
import model.ServiceType;
import model.Utility;
import model.ValidationService;


public class SignUpActivity extends AppCompatActivity implements DataValidator {

    /* Private Instance Variables */

    // Validation service for data validation.
    private ValidationService validationService;

    private EditText emailEditText, passwordEditText, firstNameEditText, lastNameEditText, mobileEditText;

    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // Animate the activity into the screen from the bottom.
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.pull_hold);


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.snackbar_container);

        // Clickable button for signing in redirects the user to the login activity.
        TextView txtSignIn = (TextView) findViewById(R.id.clickable_sign_in_text);
        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


        Spinner countryCodeSpinner = (Spinner) findViewById(R.id.country_code_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.country_codes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCodeSpinner.setAdapter(adapter);

        emailEditText = (EditText) findViewById(R.id.signUpEmail);
        firstNameEditText = (EditText) findViewById(R.id.first_name);
        lastNameEditText = (EditText) findViewById(R.id.last_name);
        mobileEditText = (EditText) findViewById(R.id.mobile_number);
        passwordEditText = (EditText) findViewById(R.id.signUpPassword);

        // Get login data input from the user.
        int[] editTextIds = {R.id.first_name, R.id.last_name, R.id.mobile_number, R.id.signUpEmail, R.id.signUpPassword};
        EditTextDataExtractor _extractor = new EditTextDataExtractor(this, editTextIds);

        // Register a validation service for data validation.
        validationService = (ValidationService) ApplicationService.getInstance(ServiceType.ValidationService);
        try {
            validationService.register(this, findViewById(R.id.btnSignUpInSignUp), "View.OnClickListener", _extractor);
        } catch (Exception e) {
            Log.e("ERROR", "Activity did not implement DataValidator");
        }
    }

    @Override
    public void validate(String[] data) {

        // Before validating an verifying, presence of internet connection has to be gotten.
        if (!Utility.isInternetConnected(this))
            Snackbar.make(coordinatorLayout, R.string.no_internet, Snackbar.LENGTH_LONG).show();
        else {

            // User data gotten from text fields.
            String first_name = data[0], last_name = data[1], mobile_number = data[2], email = data[3], password = data[4];

            // Perform validation and verification of the data.
            // Email validation
            boolean emailValid = validationService.validateEmail(email);
            if (!emailValid || email.equalsIgnoreCase("")) {
                emailEditText.setError("Please enter a valid email ");
                return;
            }

            // Password validation
            boolean passwordValid = validationService.validatePassword(password);
            if (!passwordValid || password.equalsIgnoreCase("")) {
                passwordEditText.setError("Please enter a valid password");
                return;
            }


            // Create a new user data and store information in database.
            AccountUser user = new AccountUser(first_name, last_name, email);
            user.setPassword(password);
            user.setMobile(CountryCodes.FRENCH, mobile_number);
            boolean successful = user.save(); // saves the user data on the server

            // logs the user into the application
            if (successful) {
                Intent intent = new Intent(this, ReservationActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    protected void onPause() {
        // When the activity is paused (i.e it is no longer visible), the activity leaves the screen by a slide
        // through the bottom of the screen.
        overridePendingTransition(R.anim.pull_hold, R.anim.slide_out_bottom);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}