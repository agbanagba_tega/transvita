package fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.transvita.R;


/**
 * A dialog fragment that shows the forgot password dialog fragment for email input.
 */
public class ForgotPasswordFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.fragment_forgot_password, null))
                .setTitle(R.string.forgot_pass)
                .setPositiveButton(R.string.done, new DoneClickListener());
        return builder.create();
    }

    /**
     * This listener is called when  a password reset request is gotten from
     */
    private class DoneClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {

            // TODO: Check the email for existence and send reset mail.
        }
    }
}