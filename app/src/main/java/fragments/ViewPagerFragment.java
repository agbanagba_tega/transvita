package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.transvita.LoginActivity;
import com.transvita.QuickReservation;
import com.transvita.R;
import com.transvita.SignUpActivity;

import adapters.ViewPagerAdapter;

public class ViewPagerFragment extends Fragment {

    /* Private Instance Variables */

    private int mKeyPosition;
    private FragmentButtonListeners buttonListeners;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the arguments passed to the fragment
        mKeyPosition = getArguments().getInt(ViewPagerAdapter.POSITION_KEY);
        buttonListeners = new FragmentButtonListeners();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View fragmentView = null;

        // For every position of the view pager slide, a different fragment layout is
        // loaded with different views in them.
        switch (mKeyPosition) {
            case 0:
                fragmentView = inflater.inflate(R.layout.pager_view_1, container, false);

                // Fragment background image
                ImageView pagerImage1 = (ImageView) fragmentView.findViewById(R.id.view_pager_image);
                pagerImage1.setImageResource(R.drawable.image001);

                // Fragment center image
                ImageView centerImage1 = (ImageView) fragmentView.findViewById(R.id.center_image);
                centerImage1.setImageResource(R.mipmap.ic_launcher);

                // Fragment login and sign up buttons
                (fragmentView.findViewById(R.id.btnLogin)).setOnClickListener(buttonListeners);
                (fragmentView.findViewById(R.id.btnSignUp)).setOnClickListener(buttonListeners);
                break;
            case 1:
                fragmentView = inflater.inflate(R.layout.pager_view_2, container, false);

                // Fragment background image
                ImageView pagerImage2 = (ImageView) fragmentView.findViewById(R.id.view_pager_image);
                pagerImage2.setImageResource(R.drawable.image007);

                // Set button listener to launch the quick reservation activity.
                (fragmentView.findViewById(R.id.btnQuickReservation)).setOnClickListener(buttonListeners);
                break;
        }
        return fragmentView;
    }


    private class FragmentButtonListeners implements View.OnClickListener {


        public void onClick(View view) {
            int viewId = view.getId();
            Intent intent = null;
            switch (viewId) {
                case R.id.btnLogin:
                    intent = new Intent(getActivity(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;
                case R.id.btnSignUp:
                    intent = new Intent(getActivity(), SignUpActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;
                case R.id.btnQuickReservation:
                    intent = new Intent(getActivity(), QuickReservation.class);
                    startActivity(intent);
                    break;
                default:
            }
        }
    }
}