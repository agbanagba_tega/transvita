package model;

import org.json.JSONObject;

/**
 * Results gotten back from a request always comes in JSON format. the JSON data is broken down into
 * an array and returned to the caller to decipher what is needed from the JSON data.
 *
 * @author Agbanagba Oghenetega
 */
public class Result {

    /* Private Instance Variables */
    private String errorMessage;

    /* Flag to know if there is an error in the request that gave this result. */
    private boolean isError = false;

    /* json object containing result */
    private JSONObject jsonObject;

    public Result() {
    }

    /**
     * Sets the error message of the result if there is an error in the request.
     *
     * @param errorMessage the error message
     */
    public void setErrorMessage(String errorMessage) {
        this.isError = true;
        this.errorMessage = errorMessage;
    }

    /**
     * Returns the error message if there is an error else it returns null.
     *
     * @return the error message.
     */
    public String getErrorMessage() {
        if (isError)
            return errorMessage;
        else
            return null;
    }

    public void setError(boolean isError) {
        throw new UnsupportedOperationException("The error flag cannot be set by outside call.");
    }

    /**
     * Sets the json object containing the result.
     * @param jsonObject the json object.
     */
    public void setJSONObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}