package model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.SimpleAdapter;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * @author Agbanagba Oghenetega
 */
public class Places {

    /**
     * Private Instance Variables
     */
    private static final String API_KEY = "AIzaSyDLHP5GXvnxqA3H8UxXWdBrICpQR1V40Ts";
    private static final String WEB_SERVICE_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/";


    /**
     *
     */
    public static class PlacesTask extends AsyncTask<String, Void, String> {

        /* Private Instance Variables */
        private Context context;
        private AutoCompleteTextView autoCompleteTextView;

        public PlacesTask(Context context, AutoCompleteTextView autoCompleteTextView) {
            this.context = context;
            this.autoCompleteTextView = autoCompleteTextView;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // TODO Start spinning progress bar.
        }

        @Override
        protected String doInBackground(String... place) {
            String data = "";
            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "UTF-8");
            } catch (UnsupportedEncodingException uex) {
                Log.e(Application.Tags.APPLICATION_ERROR, "Unsupported encoding at model.Places.PlacesTask#doInBackground()");
            }

            /** Web service parameters */
            String webServiceParams = input + "&types=geocode&language=fr&sensor=false&key=" + API_KEY;

            /** Output format of the web service request. */
            String outputFormat = "json";
            String requestURL = WEB_SERVICE_URL + outputFormat + "?" + webServiceParams;
            Log.i("Web Service URL", requestURL);
            try {
                data = Utility.downloadURL(requestURL); // Fetch data from web service.
            } catch (Exception ex) {
                Log.e(Application.Tags.APPLICATION_ERROR, "Web service places data download error at models.Places.PlacesTask#doInBackground()");
            }
            Log.i("JSON Data", data);
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // Starts the processing of the web service result(JSON).
            new ParserTask(context, autoCompleteTextView).execute(s);
        }
    }

    /**
     *
     */
    public static class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        /* Private Instance Variables */
        private Context context;
        private JSONObject jsonObject;
        private AutoCompleteTextView autoCompleteTextView;

        public ParserTask(Context context, AutoCompleteTextView autoCompleteTextView) {
            this.context = context;
            this.autoCompleteTextView = autoCompleteTextView;
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJSONParser = new PlaceJSONParser();
            try {
                jsonObject = new JSONObject(jsonData[0]);
                places = placeJSONParser.parse(jsonObject);
            } catch (Exception ex) {
                Log.e(Application.Tags.APPLICATION_ERROR, "Error parsing json in ParserTask");
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            super.onPostExecute(result);
            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(context, result, android.R.layout.simple_dropdown_item_1line, from, to);
            autoCompleteTextView.setAdapter(adapter);
//            autoCompleteTextView.showDropDown();
            Log.i("ATTV", "Adapter set");

            // TODO Stop spinning the progress bar.
        }
    }
}