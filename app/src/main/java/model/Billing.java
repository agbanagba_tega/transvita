package model;

/**
 * Billing information of a reservation made by a user in the system. The billing information contains the
 * amount to be paid by the user, the preferred payment option and the payment status.
 */
public class Billing {

    /* Private Instance Variables */


}