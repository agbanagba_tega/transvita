package model;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.transvita.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Agbanagba Oghenetega
 */
public class Request extends AsyncTask<RequestData, Void, Result> {

    /* Private Instance Variable */
    private Result result;

    /**
     * Request data for the type of request.
     */
    private String requestTag;

    /**
     * Progress dialog.
     */
    private ProgressDialog progressDialog;

    /***/
    private Context context;

    /**
     *
     * @param context
     * @param requestTag
     */
    public Request(Context context, String requestTag) {
        this.context = context;
        this.requestTag = requestTag;
    }

    @Override
    protected Result doInBackground(RequestData... requestData) {

        result = new Result();
        // Check for the request data type and get request url.

        if (requestData.length > 1) {
            result.setErrorMessage("Request Data should not exceed 1.");
        } else {

            List<Object> requestParams = requestData[0].getDataObjects(); // request parameters
            String requestTag = requestData[0].getTag(); // request tag
            String params = null;

            // TODO Add other parameters.
            switch (requestTag) {
                case Application.Tags.APPLICATION_LOGIN:
                    params = "emailAddress=" + requestParams.get(0) + "&pass=" + requestParams.get(1);
                    break;
                case Application.Tags.APPLICATION_SIGNUP:
                    params = "";
                    break;
                case Application.Tags.APPLICATION_RESERVATION:
                    params = "pickupaddress=" + requestParams.get(0) + "&destination=" + requestParams.get(1);
                    break;
                case Application.Tags.APPLICATION_QRRESERVATION:
                    params = "";
                    break;
                case Application.Tags.APPLICATION_TEST_GOOGLE:
                    params = "q=agbanagba";
            }
            try {
                accessURL(RequestAddress.getURL(requestTag), params);
            } catch (IOException e) {
                Log.i(Application.Tags.APPLICATION_URL_REQUEST_ERROR_TAG, "An io error occurred " +
                        "while trying to make a URL request.");
            } catch (JSONException jse) {
                Log.i(Application.Tags.APPLICATION_URL_REQUEST_ERROR_TAG, "An json error occurred " +
                        "while trying read to json.");
            }
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        /** A progress dialog is shown waiting for request results depending on the type of request. */
        switch (requestTag) {
            case Application.Tags.APPLICATION_LOGIN:
            case Application.Tags.APPLICATION_SIGNUP:
            case Application.Tags.APPLICATION_RESERVATION:
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getResources().getString(R.string.logging_in));
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setIndeterminate(true);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                break;
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        // Dismiss progress dialog after execution.
        progressDialog.dismiss();
    }

    /**
     * Accesses the url given in the parameter and returns a boolean flag stating whether the
     * request was successful or not.
     *
     * @param urlString the url string
     * @return a boolean
     * @throws IOException
     * @throws JSONException
     */
    private boolean accessURL(String urlString, String urlParamters) throws IOException, JSONException {
        boolean successful;
        // url parameters bytes
        byte[] urlParameterBytes = urlParamters.getBytes(StandardCharsets.UTF_8);

        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setInstanceFollowRedirects(false);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        urlConnection.setRequestProperty("charset", "UTF-8");
        urlConnection.setRequestProperty("Content-Length", Integer.toString(urlParameterBytes.length));
        urlConnection.setReadTimeout(10000);
        urlConnection.setConnectTimeout(10000);
        urlConnection.setDoInput(true);
        urlConnection.connect();

        // Write url parameters to the connection
        try (DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream())) {
            outputStream.write(urlParameterBytes);
        }

        int responseCode = urlConnection.getResponseCode();
        if (responseCode == 200) {
            successful = true;
            Log.i("Request", "Application Request Successful");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder stringbuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null)
                stringbuilder.append(line);
            JSONObject jsonObject = new JSONObject(stringbuilder.toString());
            result.setJSONObject(jsonObject);
        } else {
            Log.i("Request", "Application Request Not Successful");
            successful = false;
        }
        return successful;
    }

    /**
     *
     */
    private static class RequestAddress {

        private static final String LOGIN_URL = "https://bugzilla.mozilla.org/rest/bug?assigned_to=lhenry@mozilla.com";
        private static final String SIGNUP_URL = "/signup";
        private static final String RESERVATION_URL = "/reservation";
        private static final String QRRESERVATION_URL = "/qrReservation";
        private static final String TEST_GOOGLE_URL = "https://www.google.com";

        /**
         * Returns the url (RESTFUL) for the request type.
         *
         * @param requestType the request type.
         * @return the url for the request type.
         */
        public static String getURL(String requestType) throws NoSuchElementException {
            String url;
            switch (requestType) {
                case Application.Tags.APPLICATION_LOGIN:
                    url = LOGIN_URL;
                    break;
                case Application.Tags.APPLICATION_SIGNUP:
                    url = SIGNUP_URL;
                    break;
                case Application.Tags.APPLICATION_RESERVATION:
                    url = RESERVATION_URL;
                    break;
                case Application.Tags.APPLICATION_QRRESERVATION:
                    url = QRRESERVATION_URL;
                    break;
                case Application.Tags.APPLICATION_TEST_GOOGLE:
                    url = TEST_GOOGLE_URL;
                    break;
                default:
                    throw new NoSuchElementException("Wrong request type");
            }
            return url;
        }
    }
}