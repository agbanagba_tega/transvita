package model;

/**
 * A user can have an account or for quick reservation purposes can decide not to have an account.
 * Users information includes Basic Information, Payments Information. This class is an abstract class that holds
 * information common to both. The user class uses an ORM approach and a lazy approach to save and retrieve data
 * from the online data store.
 */
public abstract class User {

    /* Private Instance Variables */

    /* An identification for the user in the system.
     *  The identification number is auto generated.
     */
    protected long userId;

    /* The email address of the user. */
    protected String emailAddress;

    /* The phone number of the user. */
    protected String phoneNumber;

    /* The country code of the phone number. (For Transvita,
     * its +33(france) and any other european country).
     */
    protected String countryCode;

    /* The payment information of the user. */
    private PaymentsInfo paymetInfo;

    /**
     * Returns the email address of the user.
     * @return the email address of the user.
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Generates a random id for the user and then returns the generated ID.
     * @return the generated ID for the user.
     */
    public long generateID() {
        return 0;
    }

    /**
     * Returns the ID of the user.
     * @return
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Saves users data directly to the data store.
     * @return true if the save operation was successful and false otherwise.
     */
    public abstract boolean save();

    /**
     * Fetches user data based on the identification of the user.
     * @param userId the user id for perform ID searc
     * @return
     */
    public abstract User fetch(long userId);
}