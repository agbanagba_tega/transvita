package model;

/**
 * A quick reservation user is a user that makes a reservation on Transvita without an account.
 * Quick reservation user's information includes Email, PhoneNumber and Payments Information.
 */
public class QRUser extends User{

    @Override
    public boolean save() {
        throw new UnsupportedOperationException("This operation is not supported in this class.");
    }

    @Override
    public User fetch(long userId) {
        return null;
    }
}