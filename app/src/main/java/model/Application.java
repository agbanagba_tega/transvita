package model;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.widget.SimpleAdapter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.transvita.FetchAddressIntentService;
import com.transvita.R;

/**
 * A global class called before any activity is called and can hold application wide data.
 *
 * @author Agbanagba Oghenetega
 */
public class Application extends android.app.Application implements com.google.android.gms.location.LocationListener {

    /* Private Instance Variables */

    /**
     * Static user information
     */
    private static User applicationUser;

    /* The location of the user. */
    private static Location userLocation;

    /* The street/string address of the user. */
    private static String currentAddressLocation;

    private static GoogleApiClient mGoogleApiClient;

    private AddressResultReceiver mResultReceiver;

    private LocationRequest mLocationRequest;

    @Override
    public void onCreate() {
        super.onCreate();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(3 * 1000)
                .setFastestInterval(1 * 1000);

        registerActivityLifecycleCallbacks(new LifeCycleCallback());

        // Connect to Google Api Client and request for the location of the user.
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    /**
     * Sets the application user information for application wide access. On log out
     * of the application user from the application, the application user static reference
     * should be set to NULL.
     *
     * @param user the user information.
     */
    public static void setApplicationUser(User user) {
        applicationUser = user;
    }

    /**
     * Returns the application users information.
     *
     * @return the application user.
     */
    public static User getApplicationUser() {
        return applicationUser;
    }

    /**
     * @param userLocation
     */
    public static void setUserLocation(Location userLocation) {
        throw new UnsupportedOperationException("The user application cannot be set.");
    }


    /**
     * Returns the location of the user. The location of the user is gotten here because
     * the location of the user is static, location results might not come on time, so we have to get it
     * early before we decide to use it. If the location is null, the system wait for the location
     *
     * @return the location of the user.
     */
    public static Location getUserLocation() {
        if (userLocation == null) {
            // TODO wait for location to not become null.
        }
        return userLocation;
    }

    /**
     * @param currentAddress the current address of the user.
     */
    private static void setCurrentAddress(String currentAddress) {
        currentAddressLocation = currentAddress;
    }

    /**
     * Returns the current Address of the user based on the location of the user in coordinates.
     *
     * @return the address of the user.
     */
    public static String getCurrentAddressLocation() {
        return currentAddressLocation;
    }

    /**
     * Disconnects the api client in the application map activity.
     */
    public void disconnectApiClient() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    public void connectApiClient() {
        mGoogleApiClient.connect();
    }


    /**
     * Builds the google api client and adds necessary callbacks.
     */
    protected synchronized void buildGoogleApiClient() {
        GoogleApiClientConCallbacks callbacks = new GoogleApiClientConCallbacks();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(callbacks)
                .addOnConnectionFailedListener(callbacks)
                .addApi(LocationServices.API)
                .build();
    }


    /**
     * Delivers the address from the address receiver to the map activity for marker and
     * current location display.
     *
     * @param address the address to deliver.
     */
    private void deliverResultToMapActivity(String address) {
        setCurrentAddress(address);
    }

    /**
     * Begins the address look up service on a separate thread in
     * the background and waits for the result.
     */
    protected void startAddressLookupService() {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        mResultReceiver = new AddressResultReceiver(new Handler());
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, userLocation);
        this.startService(intent);
    }

    /**
     *
     */
    private class GoogleApiClientConCallbacks implements GoogleApiClient.ConnectionCallbacks,
            GoogleApiClient.OnConnectionFailedListener {

        @Override
        public void onConnected(Bundle bundle) {

            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, Application.this);
            else {
                handleLocation(location);
            }
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
        }

        @Override
        public void onConnectionSuspended(int i) {
        }
    }

    private void handleLocation(Location location) {
        userLocation = location;
        // Geocoder not present on device
        if (!Geocoder.isPresent())
            return;

        /** Start the address look up service immediately the location is ready. */
        startAddressLookupService();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleLocation(location);
    }


    /**
     * The address result receiver receives the result of the address look up
     * intent service performed on a separate thread.
     */
    private class AddressResultReceiver extends ResultReceiver {

        /* Private Instance variables */
        private String addressString;

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                addressString = resultData.getString(Constants.RESULT_DATA_KEY);
                deliverResultToMapActivity(addressString);
            } else if (resultCode == Constants.FAILURE_RESULT)
                addressString = getResources().getString(R.string.no_address_found);
        }
    }

    /**
     * Life cycle call back for application activity.
     */
    private class LifeCycleCallback implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    /**
     * Holds different tags used in the application.
     */
    public static class Tags {

        /**
         * Tags used in the application.
         */
        public static final String USER_ERROR_TAG = "user_error";
        public static final String APPLICATION_URL_REQUEST_ERROR_TAG = "url_request_error";
        public static final String APPLICATION_LOGIN = "LOGIN";
        public static final String APPLICATION_SIGNUP = "SIGNUP";
        public static final String APPLICATION_RESERVATION = "RESERVATION";
        public static final String APPLICATION_RESERVATION_CANCEL = "CANCEL RESERVATION";
        public static final String APPLICATION_QRRESERVATION = "QRRESERVATION";
        public static final String APPLICATION_TEST_GOOGLE = "google";
        public static final String APPLICATION_ERROR = "Transvita Error";
    }
}