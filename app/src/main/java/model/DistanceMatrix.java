package model;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author Agbanagba Oghenetega
 */
public class DistanceMatrix extends AsyncTask<String, Void, String> {

    /**
     * The origin address.
     */
    private String originAddress;

    /**
     * The destination address.
     */
    private String destinationAddress;

    /**
     * The distance between origin and destination in Km.
     */
    private String distance;

    /**
     * Time to move from origin to destination in mins.
     */
    private String time;

    /**
     * api key used to access web service (already configured in google developers console.
     */
    private static final String API_KEY = "AIzaSyDLHP5GXvnxqA3H8UxXWdBrICpQR1V40Ts";

    /**
     * web service url for distance matrix lookup.
     */
    private static final String WEB_SERVICE_URL = "https://maps.googleapis.com/maps/api/distancematrix/";

    /**
     * @param origin
     * @param destination
     */
    public DistanceMatrix(String origin, String destination) {
        this.originAddress = origin;
        this.destinationAddress = destination;
    }

    @Override
    protected String doInBackground(String... params) {

        /* Encode input parameters */
        try {
            originAddress = "origins=" + URLEncoder.encode(originAddress, "UTF-8");
            destinationAddress = "destinations=" + URLEncoder.encode(destinationAddress, "UTF-8");
        } catch (UnsupportedEncodingException uex) {
            Log.i(Application.Tags.APPLICATION_ERROR, "Unsupported encoding when trying to encode input parameters. ");
        }


        /** Web service parameters */
        String webServiceParams = originAddress + destinationAddress + "&mode=driving&language=fr&sensor=false&key=" + API_KEY;

        /** Output format of the web service request. */
        String outputFormat = "json";
        String requestURL = WEB_SERVICE_URL + outputFormat + "?" + webServiceParams;
        String data = ""; /* json data in string format. */
        try {
            data = Utility.downloadURL(requestURL);
        } catch (IOException ioe) {
            Log.i(Application.Tags.APPLICATION_ERROR, "IO Exception thrown models.DistanceMatrix");
        }
        return data;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);

        /** Processes json data */
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(data);
            parse(jsonObject);
        } catch (JSONException jsex) {
            Log.i(Application.Tags.APPLICATION_ERROR, jsex.toString());
        }
    }


    /**
     * Processes json distance matrix data and gets the distance and time
     * between an origin and destination address.
     *
     * @param jsonObject the json object to process.
     * @throws JSONException
     */
    public void parse(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray("rows").getJSONArray(0);
        Log.i(Application.Tags.APPLICATION_ERROR, jsonArray.toString());
        JSONObject distanceObject;
        this.distance = jsonObject.getString("text");

        JSONObject timeObject;
        this.time = jsonObject.getString("text");
    }
}