package model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Agbanagba Oghenetega
 */
public class PlaceJSONParser {

    /**
     * Receives a json object and
     *
     * @param jsonObject the json object.
     * @return a list containing places data.
     */
    public List<HashMap<String, String>> parse(JSONObject jsonObject) {
        JSONArray jplaces = null;
        try {

            /** get all elements in the places array. */
            jplaces = jsonObject.getJSONArray("predictions");
        } catch (JSONException jsonex) {
            Log.e(Application.Tags.APPLICATION_ERROR, "Error parsing JSON Array from model.PlaceJSONParser#parse()");
        }

        return getPlaces(jplaces);
    }

    /**
     * @param jsonArray
     * @return
     */
    private List<HashMap<String, String>> getPlaces(JSONArray jsonArray) {
        int placesCount = jsonArray.length();
        List<HashMap<String, String>> placesList = new ArrayList<>();
        for (int i = 0; i < placesCount; i++) {
            try {
                placesList.add(getPlace((JSONObject) jsonArray.get(i)));
            } catch (JSONException jsonex) {
                Log.e(Application.Tags.APPLICATION_ERROR, "Error parsing JSON Array from model.PlaceJSONParser#getPlaces()");
            }
        }
        return placesList;
    }

    /**
     * @param jsonObject
     * @return
     */
    private HashMap<String, String> getPlace(JSONObject jsonObject) {
        HashMap<String, String> place = new HashMap<>();
        String id, reference, description;
        try {
            id = jsonObject.getString("id");
            reference = jsonObject.getString("reference");
            description = jsonObject.getString("description");

            place.put("_id", id);
            place.put("reference", reference);
            place.put("description", description);

        } catch (JSONException jsonex) {
            Log.e(Application.Tags.APPLICATION_ERROR, "Error parsing JSON Array from model.PlaceJSONParser#getPlace()");
        }
        return place;
    }
}