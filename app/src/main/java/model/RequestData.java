package model;

import java.util.ArrayList;
import java.util.List;

/**
 * The request data class sends and receives data from a datasource. Calls to and from the datasource uses
 * a RESTFul API Call to the online server to pull data. Data that can be pulled and pushed through the application
 * includes : User information, Reservation information etc.
 *
 * @author Agbanagba Oghenetega
 */
public class RequestData {

    /* Private Instance variables */
    private String tag;

    /**
     * The data params for the request data. Each data in the arraylist is of type
     *
     * @link {java.lang.Object} and should be converted to their respective types
     * when being used.
     */
    private List<Object> data;

    /**
     * The default constructor of the request data which takes the tag of the request data.
     *
     * @param tag the tag of this request
     */
    public RequestData(String tag) {
        this.tag = tag;
    }

    /**
     * Takes data params for the request. The number of data params are unknown for a
     * type of request. The tag represents the type of request data is coming in.
     *
     * @param objects the data for the tag
     */
    public void params(Object... objects) {
        data = new ArrayList<>();
        for (Object object : objects)
            data.add(object);
    }

    public void setTag(String tag) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("The tag for the request data cannot be changed.");
    }

    /**
     * Returns the request tag for the data.
     *
     * @return request tag.
     */
    public String getTag() {
        return tag;
    }

    /**
     * Returns the data objects for this request data object.
     *
     * @return the data object.
     */
    public List<Object> getDataObjects() {
        return data;
    }
}