package model;


/**
 * Contains all reservation information made by a user in the system.
 */
public class Reservation {

    /* Private Instance Variables */

    /* The account of the user that made the reservation */
    private Account account;

    /* The billing information of the reservation. */
    private Billing billing;

    /* The pick up location */
    private String pickup_Address;

    /* The desination location of the reservation. */
    private String destination_Address;

    /* The distance between the pick up location and the destination location.
     *  This is used for pricing of reservation.
     */
    private float distance;


    public Reservation(){

    }

}
