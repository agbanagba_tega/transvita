package model;

/**
 * Account users in transvita are users that create accounts on the system which are different from
 * quick reservation users. They have full access to the application.
 */
public class AccountUser extends User {

    /* Private Instance Variables */

    /* The first name of the user. */
    private String firstName;
    private String lastName;

    /* The account user's login password. */
    private String password;

    private boolean verified = false;

    public AccountUser(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = email;
    }

    /**
     * Returns the first name of the user.
     *
     * @return the first name of the user.
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Returns the last name of the user.
     *
     * @return the last name of the user.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns the full name of the user as a combination of the first name and the last name.
     *
     * @return the full name of the user.
     */
    public String getFullName() {
        return lastName + " " + firstName;
    }

    /**
     * Sets the password of the user.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the password of the user.
     *
     * @return the password of the user.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the mobile number of the user(Only french numbers allowed) + 33
     *
     * @param mobile
     */
    public void setMobile(String countryCode, String mobile) {
        this.countryCode = countryCode;
        this.phoneNumber = countryCode + mobile;
    }

    /**
     * Returns the mobile number of the user.
     *
     * @return the mobile number of the user.
     */
    public String getMobile() {
        return phoneNumber;
    }

    @Override
    public boolean save() {
        return false;
    }

    @Override
    public User fetch(long userId) {
        return null;
    }
}