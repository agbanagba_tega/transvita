package adapters;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import fragments.ViewPagerFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    /* Private Instance Variables */

    /* The number of pages that the view pager has.*/
    private int num_pages;

    /* The key used to put and get value of the position of the view pager, used by the view pager fragment. */
    public static final String POSITION_KEY = "com.transvita.adapters.POSITION_KEY";

    public ViewPagerAdapter(FragmentManager fm, int num_pages) {
        super(fm);
        this.num_pages = num_pages;
    }

    @Override
    public Fragment getItem(int position) {
        ViewPagerFragment pagerFragment = new ViewPagerFragment();
        // Holds the position of the view pager to be passed to the pager fragment.
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION_KEY, position);
        // Set the arguments of the view pager fragment to be used when re-instating fragment state.
        pagerFragment.setArguments(bundle);
        return pagerFragment;
    }

    @Override
    public int getCount() {
        return num_pages;
    }
}
