package adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.transvita.NavListClickListener;
import com.transvita.R;

import model.AccountUser;

/**
 * A view adapter for the recycler view in the navigation drawer
 * in the reservation activity.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.AdapterViewHolder> {


    /* Private Instance Variables */

    /* The user whose data will be shown in the header of the navigation drawer. */
    private AccountUser user;

    private String[] navTitles; // the titles of the navigation list.
    private String[] footerText;

    private int footerCounter;

    /* header view type constant. */
    private static final int VIEW_TYPE_HEADER = 0;

    /* row type view constant */
    private static final int VIEW_TYPE_ROW = 1;

    /* Row type view content 2*/
    private static final int VIEW_TYPE_ROW_2 = 2;

    private int[] navRowImages; // images that represent the navigation items.

    /* Navigation click listener for navigation row items. */
    private NavListClickListener navListClickListener;

    /**
     * @param navTitles
     * @param navRowImages
     * @param user
     */
    public RecyclerViewAdapter(String[] navTitles, int[] navRowImages, AccountUser user, String[] footerText) {
        this.navTitles = navTitles;
        this.navRowImages = navRowImages;
        this.user = user;
        this.footerText = footerText;
    }

    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AdapterViewHolder viewHolder;
        if (viewType == VIEW_TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header, parent, false);
            viewHolder = new AdapterViewHolder(view, viewType);
        } else if (viewType == VIEW_TYPE_ROW) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_row_item, parent, false);
            viewHolder = new AdapterViewHolder(view, viewType);
            view.setOnClickListener(navListClickListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_row_item_2, parent, false);
            viewHolder = new AdapterViewHolder(view, viewType);
            view.setOnClickListener(navListClickListener);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterViewHolder holder, int position) {
        if (holder.holderId == 0) {
            holder.userName.setText(user.getFullName());
            holder.usersEmail.setText(user.getEmailAddress());
        } else if (holder.holderId == 1) {
            holder.navRowText.setText(navTitles[position - 1]);
            holder.navRowImage.setImageResource(navRowImages[position - 1]);
        } else {
            holder.footerText.setText(footerText[footerCounter++]);
        }
    }

    @Override
    public int getItemCount() {
        // The number of items is the number navigation titles length including the header view.
        return navTitles.length + 1;
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return VIEW_TYPE_HEADER;
        else if (isPositionFooter(position))
            return VIEW_TYPE_ROW_2;
        return VIEW_TYPE_ROW;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == 2;
    }

    /**
     * Sets the navigation click listener for nav row items.
     *
     * @param listener listener that listens to navigation row click events.
     */
    public void setViewClickListener(NavListClickListener listener) {
        this.navListClickListener = listener;
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {

        int holderId;

        /* Text views holding users name and email address in the header view of the navigation drawer. */
        TextView usersEmail, userName;

        /* Text to be displayed as the text in the navigation drawer. */
        TextView navRowText;
        ImageView navRowImage;

        /* Users profile image in the circle image view. */
        ImageView profileImage;

        /* Text to be displayed at the footer of the navigation drawer. */
        TextView footerText;


        public AdapterViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == VIEW_TYPE_HEADER) {
                userName = (TextView) itemView.findViewById(R.id.usersName);
                usersEmail = (TextView) itemView.findViewById(R.id.usersEmail);
                profileImage = (ImageView) itemView.findViewById(R.id.circleImageView);
                holderId = VIEW_TYPE_HEADER;
            } else if (viewType == VIEW_TYPE_ROW) {
                navRowText = (TextView) itemView.findViewById(R.id.navRowText);
                navRowImage = (ImageView) itemView.findViewById(R.id.navRowImage);
                holderId = VIEW_TYPE_ROW;
            } else {
                footerText = (TextView) itemView.findViewById(R.id.footerText);
                holderId = VIEW_TYPE_ROW_2;
            }
        }
    }
}