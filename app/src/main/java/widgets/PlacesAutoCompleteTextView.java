package widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import java.util.HashMap;

/**
 *
 * Places auto complete textview is an editable text view that allows for the display of
 * google places information
 * @author Agbanagba Oghenetega
 */
public class PlacesAutoCompleteTextView extends AutoCompleteTextView {

    public PlacesAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        HashMap<String,String> hm = (HashMap)selectedItem;
        return hm.get("description");
    }
}